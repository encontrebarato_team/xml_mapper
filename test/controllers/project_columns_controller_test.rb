require 'test_helper'

class ProjectColumnsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @project_column = project_columns(:one)
  end

  test "should get index" do
    get project_columns_url
    assert_response :success
  end

  test "should get new" do
    get new_project_column_url
    assert_response :success
  end

  test "should create project_column" do
    assert_difference('ProjectColumn.count') do
      post project_columns_url, params: { project_column: { name: @project_column.name, project_id: @project_column.project_id, required: @project_column.required } }
    end

    assert_redirected_to project_column_url(ProjectColumn.last)
  end

  test "should show project_column" do
    get project_column_url(@project_column)
    assert_response :success
  end

  test "should get edit" do
    get edit_project_column_url(@project_column)
    assert_response :success
  end

  test "should update project_column" do
    patch project_column_url(@project_column), params: { project_column: { name: @project_column.name, project_id: @project_column.project_id, required: @project_column.required } }
    assert_redirected_to project_column_url(@project_column)
  end

  test "should destroy project_column" do
    assert_difference('ProjectColumn.count', -1) do
      delete project_column_url(@project_column)
    end

    assert_redirected_to project_columns_url
  end
end
