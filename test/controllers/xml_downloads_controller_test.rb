require 'test_helper'

class XmlDownloadsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @xml_download = xml_downloads(:one)
  end

  test "should get index" do
    get xml_downloads_url
    assert_response :success
  end

  test "should get new" do
    get new_xml_download_url
    assert_response :success
  end

  test "should create xml_download" do
    assert_difference('XmlDownload.count') do
      post xml_downloads_url, params: { xml_download: { file_size: @xml_download.file_size, observations: @xml_download.observations, path: @xml_download.path, status: @xml_download.status, test: @xml_download.test, xml_errors: @xml_download.xml_errors, xml_id: @xml_download.xml_id } }
    end

    assert_redirected_to xml_download_url(XmlDownload.last)
  end

  test "should show xml_download" do
    get xml_download_url(@xml_download)
    assert_response :success
  end

  test "should get edit" do
    get edit_xml_download_url(@xml_download)
    assert_response :success
  end

  test "should update xml_download" do
    patch xml_download_url(@xml_download), params: { xml_download: { file_size: @xml_download.file_size, observations: @xml_download.observations, path: @xml_download.path, status: @xml_download.status, test: @xml_download.test, xml_errors: @xml_download.xml_errors, xml_id: @xml_download.xml_id } }
    assert_redirected_to xml_download_url(@xml_download)
  end

  test "should destroy xml_download" do
    assert_difference('XmlDownload.count', -1) do
      delete xml_download_url(@xml_download)
    end

    assert_redirected_to xml_downloads_url
  end
end
