require 'test_helper'

class XmlColumnsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @xml_column = xml_columns(:one)
  end

  test "should get index" do
    get xml_columns_url
    assert_response :success
  end

  test "should get new" do
    get new_xml_column_url
    assert_response :success
  end

  test "should create xml_column" do
    assert_difference('XmlColumn.count') do
      post xml_columns_url, params: { xml_column: { name: @xml_column.name, number_fix: @xml_column.number_fix, regex: @xml_column.regex, xml_id: @xml_column.xml_id } }
    end

    assert_redirected_to xml_column_url(XmlColumn.last)
  end

  test "should show xml_column" do
    get xml_column_url(@xml_column)
    assert_response :success
  end

  test "should get edit" do
    get edit_xml_column_url(@xml_column)
    assert_response :success
  end

  test "should update xml_column" do
    patch xml_column_url(@xml_column), params: { xml_column: { name: @xml_column.name, number_fix: @xml_column.number_fix, regex: @xml_column.regex, xml_id: @xml_column.xml_id } }
    assert_redirected_to xml_column_url(@xml_column)
  end

  test "should destroy xml_column" do
    assert_difference('XmlColumn.count', -1) do
      delete xml_column_url(@xml_column)
    end

    assert_redirected_to xml_columns_url
  end
end
