require 'test_helper'

class ColumnMapsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @column_map = column_maps(:one)
  end

  test "should get index" do
    get column_maps_url
    assert_response :success
  end

  test "should get new" do
    get new_column_map_url
    assert_response :success
  end

  test "should create column_map" do
    assert_difference('ColumnMap.count') do
      post column_maps_url, params: { column_map: { project_column_id: @column_map.project_column_id, xml_column_id: @column_map.xml_column_id } }
    end

    assert_redirected_to column_map_url(ColumnMap.last)
  end

  test "should show column_map" do
    get column_map_url(@column_map)
    assert_response :success
  end

  test "should get edit" do
    get edit_column_map_url(@column_map)
    assert_response :success
  end

  test "should update column_map" do
    patch column_map_url(@column_map), params: { column_map: { project_column_id: @column_map.project_column_id, xml_column_id: @column_map.xml_column_id } }
    assert_redirected_to column_map_url(@column_map)
  end

  test "should destroy column_map" do
    assert_difference('ColumnMap.count', -1) do
      delete column_map_url(@column_map)
    end

    assert_redirected_to column_maps_url
  end
end
