class CreateXmlColumns < ActiveRecord::Migration[5.0]
  def change
    create_table :xml_columns do |t|
      t.string :name
      t.integer :xml_id
      t.string :regex
      t.boolean :number_fix
      t.timestamps
    end
  end
end
