class CreateXmls < ActiveRecord::Migration[5.0]
  def change
    create_table :xmls do |t|
      t.string :url
      t.integer :store_id
      t.string :store_name
      t.integer :version
      t.boolean :remove_namespaces
      t.string :encoding
      t.string :main_tag
      t.text :custom_fields
      t.boolean :validated
      t.integer :project_id

      t.timestamps
    end
  end
end
