class CreateColumnMaps < ActiveRecord::Migration[5.0]
  def change
    create_table :column_maps do |t|
      t.integer :xml_column_id
      t.integer :project_column_id
      t.timestamps
    end
  end
end
