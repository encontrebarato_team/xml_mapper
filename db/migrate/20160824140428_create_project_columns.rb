class CreateProjectColumns < ActiveRecord::Migration[5.0]
  def change
    create_table :project_columns do |t|
      t.integer :project_id
      t.string :name
      t.boolean :required
      t.timestamps
    end
  end
end
