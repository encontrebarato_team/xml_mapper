class CreateXmlDownloads < ActiveRecord::Migration[5.0]
  def change
    create_table :xml_downloads do |t|
      t.integer :xml_id
      t.integer :status, :limit => 2
      t.boolean :test
      t.text :xml_errors
      t.string :path
      t.text :observations
      t.integer :file_size
      t.string :file_md5

      t.timestamps
    end
  end
end
