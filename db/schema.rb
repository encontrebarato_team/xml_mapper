# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160824140528) do

  create_table "column_maps", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "xml_column_id"
    t.integer  "project_column_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "project_columns", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "project_id"
    t.string   "name"
    t.boolean  "required"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "projects", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.text     "description", limit: 65535
    t.string   "url"
    t.string   "api_url"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "xml_columns", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.integer  "xml_id"
    t.string   "regex"
    t.boolean  "number_fix"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "xml_downloads", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "xml_id"
    t.string   "status"
    t.boolean  "test"
    t.text     "xml_errors",   limit: 65535
    t.string   "path"
    t.text     "observations", limit: 65535
    t.integer  "file_size"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "xmls", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "url"
    t.integer  "store_id"
    t.integer  "version"
    t.boolean  "remove_namespaces"
    t.string   "encoding"
    t.string   "main_tag"
    t.text     "custom_fields",     limit: 65535
    t.boolean  "validated"
    t.integer  "project_id"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

end
