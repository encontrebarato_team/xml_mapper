# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->
  max_fields = 10
  #maximum columns
  wrapper = $('.project_columns_input_fields_wrap')
  #Fields wrapper
  add_button = $('.add_field_button')
  #Add button ID
  x = 1
  #initlal text box count
  $(add_button).click (e) ->
    #on add input button click
    e.preventDefault()
    x++ #text box increment
    new_project_column = "<tr><td><input type='text' name='project[new_columns]["+x+"][name]' id='name' value='new column' class='form-control'></td><td><a href='#' class='project_column_required_toggle' data-toggle='class'><i class='fa fa-check text-success text-active'></i><i class='fa fa-times text-danger text'></i><input type='hidden' name='project[new_columns]["+x+"][required]' value='0'></a></td><td><a class='project_column_destroy btn btn-default btn-xs btn-danger' rel='nofollow' href='#'>Destroy<input type='hidden' value='0'></a></td></tr>"
    $($('.project_columns_input_fields_wrap').find("tr").last()).after $(new_project_column)
  #user click on remove text
  $(wrapper).on 'click', '.remove_field', (e) ->
    e.preventDefault()
    $(this).parent('div').remove()
    x--

  #Batch Actions for Project List
  $("#project_action_button").on 'click', (e) ->
    e.preventDefault()
    data = $("#project_batch_form").serialize()
    $.ajax "/project_batch_action",
      type: "GET"
      async: true
      cache: false
      data: data # all data will be passed here
      success: (data) ->
        if($("#project_batch_action_selector").val() == 'delete')
          $(".project_checkbox:checked").parents("tr").remove()

  #Ajax Project Column Removal
  $(document).on 'click','.project_column_destroy' , (e) ->
    e.preventDefault()
    current_row = $(this).parent("td").parent("tr")
    if confirm "Are you sure you want to delete this column?"
      id = $(this).find("input").val()
      if id != '0'
        data = {id: id}
        $.ajax "/destroy_project_column",
          type: "GET"
          async: true
          cache: false
          data: data # all data will be passed here
          success: (data) ->
              $(current_row).remove()
      else
        $(current_row).remove()

  #Project form column required toggle
  $(document).on 'click', ".project_column_required_toggle", (e) ->
    value = $(this).find('input').val()
    $(this).find('input').val(if value == "1" then "0" else "1")
