# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
  getStores()

  $("#xml_url").on "change",->
    if($("#xml_url").val())
      $("#download_xml").prop("disabled",false)
      $("#test_xml").prop("disabled",false)
    else
      $("#download_xml").prop("disabled",true)
      $("#test_xml").prop("disabled",true)

  $("#download_xml").on "click", ->
    name = $("#xml_store_id :selected").text()
    $("#xml_store_name").val(name)
    downloadXml()
    $("#xml_url").prop("disabled",true)
    $("#xml_project_id").prop("disabled",true)
    $("#xml_store_id").prop("disabled",true)



  $("#set_main_tag").on "click", ->
    step3()
    $('.form_label').hide();
    $("#sample_text").hide();
    $("#xml_url").hide();
    $("#xml_project_id").hide();
    $("#xml_store_id").hide();
    $("#xml_main_tag").hide();
    $("#set_main_tag").prop("disabled",true).hide();
    $("#map_tags").prop("disabled",false)
    $("#map_tags").prop("hidden",false)


  $("#map_tags").on "click", ->
    step4()
    $("#map_tags").prop("disabled",true).hide()
    $("#parse_xml").prop("disabled",false)
    $("#parse_xml").prop("hidden",false)


    
  $("#parse_xml").on "click", ->
    step5()



  #Batch Actions for XML List
  $("#xml_action_button").on 'click', (e) ->
    e.preventDefault()
    data = $("#xml_batch_form").serialize()
    $.ajax "/xml_batch_action",
      type: "GET"
      async: true
      cache: false
      data: data # all data will be passed here
      success: (data) ->
        if($("#xml_batch_action_selector").val() == 'delete')
          $(".xmls_checkbox:checked").parents("tr").remove()

#Ajax Project Column Removal
$(document).on 'click','.xml_column_destroy' , (e) ->
  console.log("potato")
  e.preventDefault()
  current_row = $(this).parent("td").parent("tr")
  if confirm "Are you sure you want to delete this column?"
    id = $(this).find("input").val()
    if id != '0'
      data = {id: id}
      $.ajax "/destroy_xml_column",
        type: "GET"
        async: true
        cache: false
        data: data # all data will be passed here
        success: (data) ->
            $(current_row).remove()
    else




getStores = ->
  $.getJSON '/load_data/stores.json', (data) ->
    $.each data['items'], (k, v) ->
      $('#xml_store_id').append '<option value=' + v['id'] + '>' + v['name'] + '</option>'
      return
    return
  return

downloadXml = ->
  data = $('#new_xml').serialize()
  $.ajax
    url: '/xmls.json'
    type: 'POST'
    cache: false
    data: data
    success: (result) ->
      $("#xml_id").val(result.id)
      $.ajax
        url: '/xml/download_xml'
        type: 'GET'
        cache: false
        data: result
        success: (data) ->
          step2(data)
          return
      return
      return
  return


step2 = (result)->
        $("#sample_text").prop("hidden",false)
        $('#sample_text').val result
        $('#xml_main_tag').prop("disabled",false)
        $('#xml_main_tag').prop("hidden",false)
        $("#download_xml").prop("disable",true)
        $("#download_xml").prop("hidden",true)
        $("#test_xml").prop("disable",true)
        $("#test_xml").prop("hidden",true)
        $("#set_main_tag").prop("hidden",false)
        $("#set_main_tag").prop("disabled",false)


step3 = ->
        data = $('#new_xml').serialize()
        $.ajax
          url: '/xml/xml_tags'
          type: 'GET'
          cache: false
          data: data
          success: (result) ->
            console.log result
            $("#check_box").replaceWith(result)
            return
        return


step4 = ->
  id = $("#xml_id").val();
  data ={}
  data['id'] = id
  console.log id
  $.ajax
    url: '/xml/map_xml'
    type: 'GET'
    cache: false
    data: data
    success: (result) ->
      $(".new_xml_column").hide()
      console.log result
      $("#list_box").replaceWith(result)
      return
  return
  return

step5 = ->
  id = $("#xml_id").val();
  data ={}
  data['id'] = id
  console.log id
  $.ajax
    url: '/load_data/parse_xml.html'
    type: 'GET'
    cache: false
    data: data
    success: (result) ->

      return
  return
  return
