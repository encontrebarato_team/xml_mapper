class XmlDownloadWorker
  include Sidekiq::Worker

  def perform xml_download_id, xml_id
    xml_download = XmlDownload.find(xml_download_id)
    xml = Xml.find(xml_id)

    #File Download
    begin
      uri = URI.parse(xml.url)
      file_path = Rails.root.join('tmp','xmls',xml.id.to_s+'_xml_'+DateTime.now.to_i.to_s+'.xml')
      Net::HTTP.start(uri.host) do |http|
        begin
          xml_download.downloading!
          response = http.request_head(xml.url)
          xml_download.file_size = response['content-length']
          file = open(file_path, 'wb')
          http.request_get(uri.request_uri) do |response|
            response.read_body do |segment|
              file.write(segment)
            end
          end
        ensure
          file.close
        end
      end
    rescue Exception => e
      xml_download.xml_errors = e.to_s
      xml_download.interrupted!
      raise e
      return false
    end
    xml_download.path = file_path.to_s
    if xml_download.is_a_new_xml
      xml.increase_version
      xml_download.parsing!
    else
      File.delete(file_path) if File.exist?(file_path)
      xml_download.done!
    end
  end
end
