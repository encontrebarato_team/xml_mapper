class Project < ApplicationRecord
  #Model Relationships
  has_many :xmls
  has_many :project_columns
  has_many :memberships, :dependent => :delete_all
  
  accepts_nested_attributes_for :project_columns
end
