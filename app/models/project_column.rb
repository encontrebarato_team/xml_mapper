class ProjectColumn < ApplicationRecord
  #Model Relationships
  belongs_to :project
  has_many :column_maps
  has_many :xml_columns, through: :column_maps
end
