class XmlDownload < ApplicationRecord

  require "nokogiri"
  #Model Relationships
  belongs_to :xml
  #status values
  enum status: [ :downloading, :parsing, :done, :interrupted ]

  #Verify if XML file is the same
  def is_a_new_xml
    last_xml_download = XmlDownload.where(xml_id: self.xml_id).order(id: :desc).second
    if last_xml_download.blank?
      self.file_md5 = Digest::MD5.file self.path
      self.parsing!
      return true
    end
    self.file_md5 = Digest::MD5.file self.path
    if last_xml_download.file_md5 == self.file_md5
      File.delete(self.path) if File.exist?(self.path)
      self.observations = "XML file hasn't changed"
      self.done!
      return false
    else
      self.parsing!
      return true
    end
  end

  def first_chars
    if (f = File.open(self.path))
      return f.read(3000)
    else
      return nil
    end
  end

  def self.xml_tags(path, main_tag)
    tags = []
    xml_path = path
    main ="//#{main_tag}"
    @doc = Nokogiri::XML(File.read(xml_path))
    products = @doc.xpath("//#{main}").take(100).each do |tag|
      tag.children.each do |node|
        tags << node.name unless tags.include? node.name
      end
     end
     return tags
  end

end
