class Xml < ApplicationRecord
  #Model Relationships
  belongs_to :project
  has_many :xml_downloads
  has_many :xml_columns
  #has_many :memberships, :dependent => :delete_all

  #Parse XML file
  def parse_xml
    @observation_hash = {success: 0, fail: 0, total: 0}
    xml_download = self.download
    if xml_download.is_a_new_xml
      self.increase_version
      xml_download.parsing!
      entries = []
      data = Nokogiri::XML::Reader(File.open(xml_download.path))
      data.each do |node|
        if(node.name == self.main_tag && node.node_type == Nokogiri::XML::Reader::TYPE_ELEMENT)
          begin
            entry = self.parse_columns(Nokogiri::XML(node.outer_xml))
            entries << entry unless entry.blank?
            if entries.length >= 100
              #self.save_entries self.project.api_url, entries
              entries = []
            end
          rescue Exception => e
            @observation_hash[:fail] += 1
            xml_download.update_column(:xml_errors, e.message)
            puts "error #{e.message}"
            puts Nokogiri::XML(node.outer_xml)
          end
        end
      end
      xml_download.observations = ""
      @observation_hash.each do |key,value|
        xml_download.observations += "#{key}: #{value}. "
      end
      xml_download.done!
    end
  end

  #Parse each XML column for an entry
  def parse_columns unparsed_entry
    entry = {}
    required_columns = []
    @observation_hash[:total] += 1
    self.project.project_columns.includes(:xml_columns).each do |column|
      related_xml_column = column.xml_columns.first
      unless related_xml_column.blank?
        #Content inside xml tag
        column_value = unparsed_entry.xpath("//#{related_xml_column.name}")[0].inner_text
        regex = Regexp.new related_xml_column.regex
        column_value = Xml::apply_column_regex column_value, regex
        column_value = Xml::number_fix column_value if related_xml_column.number_fix
        entry[column.name] = column_value
        required_columns << column.name.to_sym if column.required
      end
    end
    if required_columns.all? {|c| entry[c].present?}
      @observation_hash[:success] += 1
      return entry
    else
      @observation_hash[:fail] += 1
      puts "Not all required columns filled"
      return nil
    end
  end

  #Download XML file from URL
  def download
    FileUtils.mkdir_p("tmp/xmls") unless File.directory?("tmp/xmls")
    xml_download = XmlDownload.create(xml_id: self.id)
    #XmlDownloadWorker.perform_async(xml_download.id, self.id)
    #File Download
    begin
      uri = URI.parse(self.url)
      file_path = Rails.root.join('tmp','xmls',self.id.to_s+'_xml_'+DateTime.now.to_i.to_s+'.xml')
      xml_download.path = file_path.to_s
      Net::HTTP.start(uri.host) do |http|
        begin
          xml_download.downloading!
          response = http.request_head(self.url)
          xml_download.file_size = response['content-length']
          file = open(file_path, 'wb')
          http.request_get(uri.request_uri) do |response|
            response.read_body do |segment|
              file.write(segment)
            end
          end
        ensure
          file.close
        end
      end
    rescue Exception => e
      xml_download.xml_errors = e.to_s
      xml_download.interrupted!
      raise e
      return false
    end
    return xml_download
  end

  #Test url without having to save a XML model first
  def self.test_url url
    Xml.new(url: url).download
  end

  #XML Version increment
  def increase_version
    self.version = self.version.nil? ? 1 : self.version + 1
    self.save
  end

  def self.number_fix number
    number.gsub("BRL","").strip.to_f
  end

  def self.apply_column_regex string, regex
    return regex.match string unless regex.blank?
  end

end
