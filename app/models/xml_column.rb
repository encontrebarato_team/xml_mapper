class XmlColumn < ApplicationRecord
  belongs_to :xml
  has_many :column_maps
  has_many :project_columns, through: :column_maps
  has_many :memberships, :dependent => :delete_all
end
