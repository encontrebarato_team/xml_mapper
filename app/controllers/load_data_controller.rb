class LoadDataController < ApplicationController


  def stores
    stores = HTTParty.post(Base_uri + '/load_data/stores', headers: {"Authorization" => Token})
    respond_to do |format|
      format.json { render :json => stores }
    end
  end

  def parse_xml
    @xml = Xml.find(params["id"])
    if @xml
      @xml.parse_xml

      respond_to do |format|
        format.html { redirect_to xmls_url, notice: 'Xml was successfully parsed.' }
        format.json { render :json => {status: :ok}  }
      end
    else
      respond_to do |format|
        format.html { redirect_to xmls_url, notice: 'Xml was not parsed.' }
        format.json { render :json => {status: :unprocessable_entity} }
      end
    end
  end



end
