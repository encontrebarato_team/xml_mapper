class ProjectColumnsController < ApplicationController
  before_action :set_project_column, only: [:show, :edit, :update, :destroy]

  # GET /project_columns
  # GET /project_columns.json
  def index
    @project_columns = ProjectColumn.all
  end

  # GET /project_columns/1
  # GET /project_columns/1.json
  def show
  end

  # GET /project_columns/new
  def new
    @project_column = ProjectColumn.new
  end

  # GET /project_columns/1/edit
  def edit
  end

  # POST /project_columns
  # POST /project_columns.json
  def create
    @project_column = ProjectColumn.new(project_column_params)

    respond_to do |format|
      if @project_column.save
        format.html { redirect_to @project_column, notice: 'Project column was successfully created.' }
        format.json { render :show, status: :created, location: @project_column }
      else
        format.html { render :new }
        format.json { render json: @project_column.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /project_columns/1
  # PATCH/PUT /project_columns/1.json
  def update
    respond_to do |format|
      if @project_column.update(project_column_params)
        format.html { redirect_to @project_column, notice: 'Project column was successfully updated.' }
        format.json { render :show, status: :ok, location: @project_column }
      else
        format.html { render :edit }
        format.json { render json: @project_column.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /project_columns/1
  # DELETE /project_columns/1.json
  def destroy
    @project_column.destroy
    respond_to do |format|
      format.html { redirect_to project_columns_url, notice: 'Project column was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project_column
      @project_column = ProjectColumn.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def project_column_params
      params.require(:project_column).permit(:project_id, :name, :required)
    end
end
