class XmlDownloadsController < ApplicationController
  before_action :set_xml_download, only: [:show, :edit, :update, :destroy]

  # GET /xml_downloads
  # GET /xml_downloads.json
  def index
    @xml_downloads = XmlDownload.all
  end

  # GET /xml_downloads/1
  # GET /xml_downloads/1.json
  def show
  end

  # GET /xml_downloads/new
  def new
    @xml_download = XmlDownload.new
  end

  # GET /xml_downloads/1/edit
  def edit
  end

  # POST /xml_downloads
  # POST /xml_downloads.json
  def create
    @xml_download = XmlDownload.new(xml_download_params)

    respond_to do |format|
      if @xml_download.save
        format.html { redirect_to @xml_download, notice: 'Xml download was successfully created.' }
        format.json { render :show, status: :created, location: @xml_download }
      else
        format.html { render :new }
        format.json { render json: @xml_download.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /xml_downloads/1
  # PATCH/PUT /xml_downloads/1.json
  def update
    respond_to do |format|
      if @xml_download.update(xml_download_params)
        format.html { redirect_to @xml_download, notice: 'Xml download was successfully updated.' }
        format.json { render :show, status: :ok, location: @xml_download }
      else
        format.html { render :edit }
        format.json { render json: @xml_download.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /xml_downloads/1
  # DELETE /xml_downloads/1.json
  def destroy
    @xml_download.destroy
    respond_to do |format|
      format.html { redirect_to xml_downloads_url, notice: 'Xml download was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_xml_download
      @xml_download = XmlDownload.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def xml_download_params
      params.require(:xml_download).permit(:xml_id, :status, :test, :xml_errors, :path, :observations, :file_size)
    end
end
