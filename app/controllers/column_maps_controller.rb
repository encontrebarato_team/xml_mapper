class ColumnMapsController < ApplicationController
  before_action :set_column_map, only: [:show, :edit, :update, :destroy]

  # GET /column_maps
  # GET /column_maps.json
  def index
    @column_maps = ColumnMap.all
  end

  # GET /column_maps/1
  # GET /column_maps/1.json
  def show
  end

  # GET /column_maps/new
  def new
    @column_map = ColumnMap.new
  end

  # GET /column_maps/1/edit
  def edit
  end

  # POST /column_maps
  # POST /column_maps.json
  def create
    @column_map = ColumnMap.new(column_map_params)

    respond_to do |format|
      if @column_map.save
        format.html { redirect_to @column_map, notice: 'Column map was successfully created.' }
        format.json { render :show, status: :created, location: @column_map }
        format.js
      else
        format.html { render :new }
        format.json { render json: @column_map.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # PATCH/PUT /column_maps/1
  # PATCH/PUT /column_maps/1.json
  def update
    respond_to do |format|
      if @column_map.update(column_map_params)
        format.html { redirect_to @column_map, notice: 'Column map was successfully updated.' }
        format.json { render :show, status: :ok, location: @column_map }
      else
        format.html { render :edit }
        format.json { render json: @column_map.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /column_maps/1
  # DELETE /column_maps/1.json
  def destroy
    @column_map.destroy
    respond_to do |format|
      format.html { redirect_to column_maps_url, notice: 'Column map was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_column_map
      @column_map = ColumnMap.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def column_map_params
      params.require(:column_map).permit(:xml_column_id, :project_column_id)
    end
end
