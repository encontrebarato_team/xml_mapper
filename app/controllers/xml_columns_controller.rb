class XmlColumnsController < ApplicationController
  before_action :set_xml_column, only: [:show, :edit, :update, :destroy]

  # GET /xml_columns
  # GET /xml_columns.json
  def index
    @xml_columns = XmlColumn.all
  end

  # GET /xml_columns/1
  # GET /xml_columns/1.json
  def show
  end

  # GET /xml_columns/new
  def new
    @xml_column = XmlColumn.new
  end

  # GET /xml_columns/1/edit
  def edit
  end

  # POST /xml_columns
  # POST /xml_columns.json
  def create
    @xml_column = XmlColumn.new(xml_column_params)

    respond_to do |format|
      if @xml_column.save
        format.html { redirect_to @xml_column, notice: 'Xml column was successfully created.' }
        format.json { render :show, status: :created, location: @xml_column }
        format.js
      else
        format.html { render :new }
        format.json { render json: @xml_column.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # PATCH/PUT /xml_columns/1
  # PATCH/PUT /xml_columns/1.json
  def update
    respond_to do |format|
      if @xml_column.update(xml_column_params)
        format.html { redirect_to @xml_column, notice: 'Xml column was successfully updated.' }
        format.json { render :show, status: :ok, location: @xml_column }
      else
        format.html { render :edit }
        format.json { render json: @xml_column.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /xml_columns/1
  # DELETE /xml_columns/1.json
  def destroy
    @xml_column.destroy
    respond_to do |format|
      format.html { redirect_to xml_columns_url, notice: 'Xml column was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_xml_column
      @xml_column = XmlColumn.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def xml_column_params
      params.require(:xml_column).permit(:name, :xml_id, :regex, :number_fix)
    end
end
