class XmlsController < ApplicationController
  before_action :set_xml, only: [:download_xml,:show, :edit, :update, :destroy]
  layout false, :only => [:xml_tags, :map_xml]

  # GET /xmls
  # GET /xmls.json
  def index
    #if params["xml_search"]
    #  @xmls = Xml.where("url LIKE (?)","%#{params['xml_search']}%")
    #else
      @xmls = Xml.all
    #end
  end

  # GET /xmls/1
  # GET /xmls/1.json
  def show
    @columns = @xml.xml_columns
  end

  # GET /xmls/new
  def new
    @xml = Xml.new
  end

  # GET /xmls/1/edit
  def edit
  end

  # POST /xmls
  # POST /xmls.json
  def create
    @xml = Xml.new(xml_params)

    respond_to do |format|
      if @xml.save
        format.html { redirect_to @xml, notice: 'Xml was successfully created.' }
        format.json { render :show, status: :created, location: @xml }
      else
        format.html { render :new }
        format.json { render json: @xml.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /xmls/1
  # PATCH/PUT /xmls/1.json
  def update
    respond_to do |format|
      if @xml.update(xml_params)
        format.html { redirect_to @xml, notice: 'Xml was successfully updated.' }
        format.json { render :show, status: :ok, location: @xml }
      else
        format.html { render :edit }
        format.json { render json: @xml.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /xmls/1
  # DELETE /xmls/1.json
  def destroy
    @xml.destroy
    respond_to do |format|
      format.html { redirect_to xmls_url, notice: 'Xml was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  def download_xml

    @xml.download

    sample = @xml.xml_downloads.first.first_chars
    xml_download_path = @xml.xml_downloads.last.path
    respond_to do |format|
      format.html { render plain: sample }
      format.json { render status: :ok, location: @xml}
    end
  end

  def xml_tags
    @xml = Xml.find(params[:xml][:id])
    @tags = XmlDownload.xml_tags(@xml.xml_downloads.last.path, params[:xml][:main_tag])
##    pp tags
##    respond_to do |format|
##      format.json { render :json => tags }
##    end
  end

  def map_xml
    @xml= Xml.find(params["id"])
    @project_columns = @xml.project.project_columns unless @xml.blank? || @xml.project.blank?
    @xml_columns = @xml.xml_columns unless @xml.blank? || @xml.xml_columns.blank?
    #if(@project_columns)
    #  @project_columns_required = @project_columns.where("required = ?",true)
    #  @project_columns_not_required = @project_columns.where("required = ?",false)
    #end
  end

  #XML Batch Actions
  def xml_batch_action
    #Delete selected xmls
    if params['batch_action'] == 'delete'
      params['xml'].each do |id|
        Xml.delete(id)
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_xml
      @xml = Xml.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def xml_params
      params.require(:xml).permit(:url, :store_id, :version, :remove_namespaces, :encoding, :main_tag, :custom_fields, :validated, :project_id,:store_name)
    end
end
