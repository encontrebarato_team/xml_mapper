class ProjectsController < ApplicationController
  before_action :set_project, only: [:show, :edit, :update, :destroy]

  # GET /projects
  # GET /projects.json
  def index
    if params["project_search"]
      @projects = Project.where("name LIKE (?)","%#{params['project_search']}%")
    else
      @projects = Project.all
    end
  end

  # GET /projects/1
  # GET /projects/1.json
  def show
    @columns = @project.project_columns
  end

  # GET /projects/new
  def new
    @project = Project.new
    @project_columns = []
  end

  # GET /projects/1/edit
  def edit
    @project_columns = @project.project_columns
  end

  # POST /projects
  # POST /projects.json
  def create
    @project = Project.new(project_params)
    respond_to do |format|
      if @project.save
        if params["project"]["new_columns"]
          params["project"]["new_columns"].each do |key, column|
            project_column = ProjectColumn.new(name: column["name"], required: column["required"])
            project_column.save
            @project.project_columns << project_column
          end
        end
        format.html { redirect_to @project, notice: 'Project was successfully created.' }
        format.json { render :show, status: :created, location: @project }
      else
        format.html { render :new }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /projects/1
  # PATCH/PUT /projects/1.json
  def update
    respond_to do |format|
      if @project.update(project_params)
        if params["project"]["columns"]
          params["project"]["columns"].each do |id, column_hash|
            project_column = ProjectColumn.find(id.to_i)
            project_column.update(name: column_hash["name"], required: column_hash["required"])
            project_column.required = 0 if column_hash["required"].blank?
            project_column.save
          end
        end

        if params["project"]["new_columns"]
          params["project"]["new_columns"].each do |key, column_hash|
            project_column = ProjectColumn.create(name: column_hash["name"], required: column_hash["required"])
            project_column.required = 0 if column_hash["required"].blank?
            project_column.save
            @project.project_columns << project_column
          end
        end
        format.html { redirect_to @project, notice: 'Project was successfully updated.' }
        format.json { render :show, status: :ok, location: @project }
      else
        format.html { render :edit }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /projects/1
  # DELETE /projects/1.json
  def destroy
    @project.destroy
    respond_to do |format|
      format.html { redirect_to projects_url, notice: 'Project was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  #Project Batch Actions
  def project_batch_action
    #Delete selected projects
    if params['batch_action'] == 'delete'
      params['projects'].each do |id|
        Project.delete(id)
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def project_params
      params.require(:project).permit(:name, :description, :url, :api_url)
    end
end
