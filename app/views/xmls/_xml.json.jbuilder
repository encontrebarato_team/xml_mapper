json.extract! xml, :id, :url, :store_id, :version, :remove_namespaces, :encoding, :main_tag, :custom_fields, :validated, :project_id, :created_at, :updated_at
json.url xml_url(xml, format: :json)