json.extract! xml_download, :id, :xml_id, :status, :test, :xml_errors, :path, :observations, :file_size, :created_at, :updated_at
json.url xml_download_url(xml_download, format: :json)