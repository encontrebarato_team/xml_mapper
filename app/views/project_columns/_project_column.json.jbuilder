json.extract! project_column, :id, :project_id, :name, :required, :created_at, :updated_at
json.url project_column_url(project_column, format: :json)