json.extract! column_map, :id, :xml_column_id, :project_column_id, :created_at, :updated_at
json.url column_map_url(column_map, format: :json)