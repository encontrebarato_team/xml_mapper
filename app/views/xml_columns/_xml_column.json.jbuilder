json.extract! xml_column, :id, :name, :xml_id, :regex, :number_fix, :created_at, :updated_at
json.url xml_column_url(xml_column, format: :json)