Rails.application.routes.draw do
  get 'home/index'

  resources :xml_columns
  resources :projects
  resources :project_columns
  resources :column_maps
  resources :xml_downloads
  resources :xmls
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'home#index'



  #Projects and Project Columns Ajax Calls
  get 'project_batch_action' => 'projects#project_batch_action'
  get 'destroy_project_column' => 'project_columns#destroy'

  #XML ajax calls
  get 'xml_batch_action' => 'xmls#xml_batch_action'
  get 'destroy_xml_column' => 'xml_columns#destroy'
  get 'load_data/stores'
  get 'xml/download_xml' => 'xmls#download_xml'
  get 'xml/xml_tags' => 'xmls#xml_tags'
  get 'xml/map_xml' => 'xmls#map_xml'
  get 'load_data/parse_xml'


end
